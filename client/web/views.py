from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings

from web.models import Publisher, Advertiser, Counter
from .forms import UserForm, PublisherForm, AdvertiserForm, CreateAdForm, CreateSlotForm
from .utils import rest_url, getLockInId, getSlotCount, getAdCount, getUserCount

from uuid import uuid4
from datetime import datetime, timedelta
import requests
from PIL import Image
import base64
from io import BytesIO


# Create your views here.
def registerAdvertiser(request):
    uform = UserForm(request.POST or None)
    aform = AdvertiserForm(request.POST or None)

    if uform.is_valid() and aform.is_valid():
        user = uform.save(commit=False)
        username = uform.cleaned_data['username']
        password = uform.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        person = aform.save(commit=False)
        person.user = user
        person.save()
        json_data = {
            "$class": "org.nitk.webadvertising.Advertiser",
            "entityName": person.entity_name,
            "userId": user.id,
            "adCoins": "0.0"
        }
        r = requests.post(rest_url("api/advertiser"), data=json_data)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('web:home')

    context = {
        "uform": uform,
        "aform": aform
    }

    return render(request, 'web/register_advertiser.html', context)


def registerPublisher(request):
    uform = UserForm(request.POST or None)
    pform = PublisherForm(request.POST or None)

    if uform.is_valid() and pform.is_valid():
        user = uform.save(commit=False)
        username = uform.cleaned_data['username']
        password = uform.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        person = pform.save(commit=False)
        person.user = user
        person.save()
        json_data = {
            "$class": "org.nitk.webadvertising.Publisher",
            "firstName": person.first_name,
            "lastName": person.last_name,
            "userId": user.id,
            "adCoins": "0.0"
        }
        r = requests.post(rest_url("api/publisher"), data=json_data)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('web:home')

    context = {
        "uform": uform,
        "pform": pform
    }

    return render(request, 'web/register_publisher.html', context)


def login_user(request):
    if request.user.is_authenticated:
        return redirect('web:home')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('web:home')
            else:
                return render(request, 'web/login.html', {'error_message': 'Your account has not been activated!'})
        else:
            return render(request, 'web/login.html', {'error_message': 'Invalid login'})
    return render(request, 'web/login.html')


def logout_user(request):
    logout(request)
    return redirect('web:home')


def index(request):
    user = request.user.id
    if request.user.is_authenticated:
        userObj = getUser(user)
    else:
        return redirect('web:login_user')

    if isinstance(userObj, Advertiser):
        return redirect('web:myads')
    else:
        return redirect('web:myslots')


def getUser(user):
    userObj = User.objects.get(id=user)
    try:
        pub = Publisher.objects.get(user=userObj)
        return pub
    except Exception as e:
        adv = Advertiser.objects.get(user=userObj)
        return adv


def slots(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    data = {
        "owner": "resource:org.nitk.webadvertising.Publisher#" + str(request.user.id)
    }
    r = requests.get(rest_url("api/queries/selectSlotByOwner"), params=data)
    response = r.json()
    context = {
        "slots": response
    }
    return render(request, 'web/my_slots.html', context)


def slot(request, slotid):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    r = requests.get(rest_url("api/Slot/") + str(slotid))
    context = {
        "slot": r.json(),
    }

    return render(request, 'web/slot.html', context)


def ads(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Publisher):
        return HttpResponse("Unauthorised access")

    data = {
        "advertiser": "resource:org.nitk.webadvertising.Advertiser#" + str(request.user.id)
    }
    r = requests.get(rest_url("api/queries/selectAdByOwner"), params=data)
    response = r.json()
    print(response)
    context = {
        "ads": response
    }
    return render(request, 'web/my_ads.html', context)


def ad(request, adid):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Publisher):
        return HttpResponse("Unauthorised access")

    r = requests.get(rest_url("api/Advertisement/") + str(adid))
    context = {
        "ad": r.json()
    }
    return render(request, 'web/ad.html', context)


def createAd(request):
    form = CreateAdForm()
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Publisher):
        return HttpResponse("Unauthorised access")

    if request.method == "POST":
        form = CreateAdForm(request.POST, request.FILES)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            image = Image.open(cleaned_data.get('image'))
            #width,height = image.size
            buffered = BytesIO()
            image.save(buffered, format="PNG")
            img_str = b"data:image/png;base64," + base64.b64encode(buffered.getvalue())
            adid = getAdCount()

            data = {
                '$class': "org.nitk.webadvertising.Advertisement",
                "advertisementId": str(adid),
                'name': cleaned_data.get('name'),
                'image': img_str,
                'maxCost': cleaned_data.get('max_cost'),
                'description': cleaned_data.get('description'),
                'targetUrl': cleaned_data.get('target_url'),
                "widthPx": 0,
                "heightPx": 0,
                "expiryDate": str((datetime.now() + timedelta(days=30)).strftime("%Y-%m-%dT%H:%M:%S.%fZ")),
                "advertiser": "resource:org.nitk.webadvertising.Advertiser#"+str(request.user.id)
            }

            r = requests.post(rest_url("api/Advertisement"), data=data)
        return redirect("web:myads")

    context = {
        "createform": form
    }

    return render(request, 'web/create_ad.html', context)


def createSlot(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    form = CreateSlotForm()

    if request.method == "POST":
        form = CreateSlotForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            slotid = getSlotCount()

            data = {
                "$class": "org.nitk.webadvertising.Slot",
                "slotId": str(slotid),
                "website": cleaned_data.get('website'),
                "onSale": True,
                "verifiedWebsite": False,
                "verifiedSlot": False,
                "generatedUrl": "http://" + settings.HOST_SERVER + ":8000/redirect/" + str(uuid4())[:8],
                "cost": cleaned_data.get("cost"),
                "widthPx": 0,
                "heightPx": 0,
                "owner": "resource:org.nitk.webadvertising.Publisher#" + str(request.user.id)
            }

            r = requests.post(rest_url("api/Slot"), data=data)

        return redirect('web:myslots')

    context = {
        "createform": form
    }
    return render(request, 'web/create_slot.html', context)


def deleteSlot(request, slotid):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    r = requests.delete(rest_url("api/Slot/") + str(slotid))

    return redirect('web:myslots')


def deleteAd(request, adid):
    form = CreateAdForm()
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Publisher):
        return HttpResponse("Unauthorised access")

    r = requests.delete(rest_url("api/Advertisement/") + str(adid))

    return redirect('web:myads')


def adCatalog(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')

    r = requests.get(rest_url("api/Advertisement"))
    response = r.json()
    context = {
        "ads": response
    }
    return render(request, 'web/ad_catalog.html', context)


def adDetails(request, adid):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    r = requests.get(rest_url("api/Advertisement/") + str(adid))
    context = {
        "ad": r.json()
    }
    return render(request, 'web/ad_details.html', context)


def transaction(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)
    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    if request.method == 'POST':
        slotid = request.POST['slotid']
        adid = request.POST['adid']

        data = {
            "$class": "org.nitk.webadvertising.AdvertisementDeal",
            "lockIn": {
                "$class": "org.nitk.webadvertising.LockIn",
                "lockInId": str(getLockInId()),
                "slot": "resource:org.nitk.webadvertising.Slot#" + str(slotid),
                "advertisement": "resource:org.nitk.webadvertising.Advertisement#" + str(adid),
                "lockedCoins": "0.0"
            },
            "slot": "resource:org.nitk.webadvertising.Slot#" + str(slotid),
            "advertisement": "resource:org.nitk.webadvertising.Advertisement#" + str(adid),
            "transactionId": "",
            "timestamp": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        }

        r = requests.post(rest_url("api/AdvertisementDeal"), json=data)
        return redirect('web:myslots')


def buyCoins(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Publisher):
        return HttpResponse("Unauthorised access")

    if request.method == 'POST':
        coins = float(request.POST['coins'])
        data = {
            "$class": "org.nitk.webadvertising.BuyAdCoins",
            "amount": coins,
            "buyer": "resource:org.nitk.webadvertising.Advertiser#" + str(request.user.id),
            "transactionId": "",
            "timestamp": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        }
        r = requests.post(rest_url("api/BuyAdCoins"), json=data)

        return redirect('web:myads')
    return render(request, 'web/buy_coins.html')


def sellCoins(request):
    if not request.user.is_authenticated:
        return redirect('web:login_user')
    obj = getUser(request.user.id)

    if isinstance(obj, Advertiser):
        return HttpResponse("Unauthorised access")

    if request.method == 'POST':
        coins = float(request.POST['coins'])
        data = {
            "$class": "org.nitk.webadvertising.SellAdCoins",
            "amount": coins,
            "buyer": "resource:org.nitk.webadvertising.Publisher#" + str(request.user.id),
            "transactionId": "",
            "timestamp": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        }
        r = requests.post(rest_url("api/SellAdCoins"), json=data)

        return redirect('web:myslots')
    return render(request, 'web/sell_coins.html')


def getAdCoinBalance(request):
    print(request)
    if not request.user.is_authenticated:
        return None
    userObj = getUser(request.user.id)

    if isinstance(userObj, Advertiser):
        r = requests.get(rest_url("api/Advertiser/") + str(request.user.id))
    else:
        r = requests.get(rest_url("api/Publisher/") + str(request.user.id))
    return HttpResponse(r.json()['adCoins'])


def getAdForSlot(request, slotid):
    data = {
        "slot": "resource:org.nitk.webadvertising.Slot#" + str(slotid)
    }
    r = requests.get(rest_url("api/queries/selectLockInBySlot"), params=data)
    res = r.json()
    try:
        return HttpResponse(res[0]['advertisement'].split("#")[1])
    except IndexError:
        return HttpResponse(-1)


def redirectService(request, url):
    full_url = "http://" + settings.HOST_SERVER + ":8000/redirect/" + url
    data = {
        "url": full_url
    }
    r = requests.get(rest_url("api/queries/selectSlotByGeneratedUrl"), params=data)

    res = r.json()
    print(res)
    if len(res):
        slotId = res[0]['slotId']
        r = requests.get("http://" + settings.HOST_SERVER + ":8000/getadforslot/" + str(slotId))
        adId = str(int(r.content))

        # Account for the click
        data = {
            "slot": "resource:org.nitk.webadvertising.Slot#" + str(slotId)
        }
        r = requests.get(rest_url("api/queries/selectLockInBySlot"), params=data)
        res = r.json()[0]
        lockInId = res['lockInId']

        data = {
            "$class": "org.nitk.webadvertising.AdClick",
            "clickTime": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")),
            "validClick": True,
            "lockIn": "resource:org.nitk.webadvertising.LockIn#" + str(lockInId),
            "transactionId": "",
            "timestamp": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        }

        r = requests.post(rest_url("api/AdClick"), json=data)

        r = requests.get(rest_url("api/Advertisement/") + adId)
        return redirect("http://" + r.json()['targetUrl'])

