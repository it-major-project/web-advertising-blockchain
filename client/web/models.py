from django.db import models


# Create your models here.
class Advertiser(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    entity_name = models.CharField(max_length=255)


class Publisher(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)


# Probably the most dumbest model I've ever created.
# But I'm taking it down. NOW.
class Counter(models.Model):
    slot_count = models.IntegerField(default=0)
    ad_count = models.IntegerField(default=0)
