from django import forms
from django.contrib.auth.models import User
from .models import Advertiser, Publisher


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email', 'username', 'password']


class AdvertiserForm(forms.ModelForm):
    entity_name = forms.CharField(max_length=255)

    class Meta:
        model = Advertiser
        fields = ['entity_name']


class PublisherForm(forms.ModelForm):
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255)

    class Meta:
        model = Publisher
        fields = ['first_name', 'last_name']


class CreateAdForm(forms.Form):
    name = forms.CharField(max_length=255)
    image = forms.FileField()
    max_cost = forms.DecimalField()
    description = forms.CharField(max_length=255)
    target_url = forms.CharField(max_length=255)


class CreateSlotForm(forms.Form):
    website = forms.CharField(max_length=255)
    cost = forms.DecimalField()
