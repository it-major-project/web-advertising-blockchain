from django.urls import path
from .views import (login_user, registerAdvertiser, registerPublisher,
                    index, slots, slot, ad, ads, logout_user, createAd, createSlot,
                    deleteSlot, deleteAd, adCatalog, adDetails, transaction,
                    buyCoins, sellCoins, getAdCoinBalance, getAdForSlot, redirectService)

app_name = 'web'

urlpatterns = [
    path('login/', login_user, name="login_user"),
    path('logout/', logout_user, name="logout_user"),
    path('register/advertiser/', registerAdvertiser, name="register_advertiser"),
    path('register/publisher/', registerPublisher, name="register_publisher"),
    path('ads/', ads, name="myads"),
    path('ad/create', createAd, name="create_ad"),
    path('ad/delete/<int:adid>', deleteAd, name="delete_ad"),
    path('ad/<int:adid>', ad, name="ad"),
    path('slots', slots, name="myslots"),
    path('slot/create', createSlot, name="create_slot"),
    path('slot/delete/<int:slotid>', deleteSlot, name="delete_slot"),
    path('slot/<int:slotid>', slot, name="slot"),
    path('catalog/', adCatalog, name="ad_catalog"),
    path('ad_detail/<int:adid>', adDetails, name="ad_details"),
    path('transaction/', transaction, name="transaction"),
    path('buycoins/', buyCoins, name="buy_coins"),
    path('sellcoins/', sellCoins, name="sell_coins"),
    path('getbalance/', getAdCoinBalance, name="adcoin_balance"),
    path('getadforslot/<int:slotid>', getAdForSlot, name="get_ad_for_slot"),
    path('redirect/<str:url>', redirectService, name="redirect"),
    path('', index, name='home'),
]
