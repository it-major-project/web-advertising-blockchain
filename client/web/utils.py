from django.conf import settings
import requests
from datetime import datetime, timedelta


def rest_url(path):
    base = settings.COMPOSER_REST_SERVER
    if path:
        base += path

    return base


def getUserCount():
    """
    Get the number of users in the platform for the userID.
    :return: int userCount
    """
    advertiserCount = len(requests.get(rest_url("api/Advertiser")).json())
    publisherCount = len(requests.get(rest_url("api/Publisher")).json())

    return advertiserCount + publisherCount


def getAdCount():
    """
    Gets the number of ads in the system so that the adId can be used.
    :return: int adCount
    """
    r = requests.get(rest_url('api/Advertisement'))
    response = r.json()
    return len(response)


def getSlotCount():
    """
    Get the number of slots in the system so that the slotId can be used.
    :return: int slotCount
    """
    r = requests.get(rest_url("api/Slot"))
    response = r.json()
    return len(response)


def getLockInId():
    """
    Get the number of lock ins in the system so that the lockInId can be used.
    :return: int lockInId
    """
    r = requests.get(rest_url("api/LockIn"))
    response = r.json()
    return len(response)


def cancelAd(adId):
    """
    This function cancels the ad and removes all the lockIns associated with the Ad.
    The ad cannot be reused, but it will be displayed in the Archives.
    :param adId:
    :return: None
    """
    # First, get all lockIns associated with the Ad
    data = {
        "advertisement": str(adId)
    }
    r = requests.get(rest_url("api/queries/selectLockInByAd"), params=data)

    responses = r.json()

    # There are multiple LockIns. Cancel all of them,
    for response in responses:

        lockInId = response['lockInId']

        data = {
            "$class": "org.nitk.webadvertising.CancelDeal",
            "invoker": str(adId),
            "lockIn": lockInId,
            "destroyDate": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")),
            "transactionId": "string",
            "timestamp": str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        }

        r_post = requests.post(rest_url("api/CancelDeal"), data=data)


def renewAd(adId):
    """
    This function will renew the ad
    :param adId:
    :return: None
    """
    r = requests.get(rest_url("api/Advertisement/") + str(adid))




def checkExpiry():
    """
    This is a periodic function which runs every 1 hour to check if any deal is expired.
    If it is, it will trigger an deal cancel transaction, thus returning the unused money
    back to the Advertiser.
    :return:
    """
    pass
