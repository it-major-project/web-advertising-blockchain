from flask import Flask
import requests
application = Flask(__name__)


@application.route("/")
def hello():
	html = getImage()
	return html

def getImage():
	slotId = 1
	r = requests.get("http://localhost:8000/getadforslot/" + str(slotId))
	adId = str(int(r.content))

	res = requests.get("http://localhost:3000/api/Advertisement/" + adId)
	data = res.json()

	r = requests.get("http://localhost:3000/api/Slot/" + str(slotId))
	url = r.json()['generatedUrl']

	html_string = "<a href=\"{}\"><img src=\"{}\"></a>".format(url, data['image'])

	return html_string

if __name__ == "__main__":
	application.run(host='0.0.0.0', port=int("8080"))


