/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Function to assign the advertisement to a slot
 * @param {org.nitk.webadvertising.AdvertisementDeal} tx
 * @transaction
 */
async function advertisementDeal(tx) {
    let slot = tx.slot;
    let advertisement = tx.advertisement;

    let advertiser = advertisement.advertiser;

    if (advertiser.adCoins < advertisement.maxCost) {
        throw new Error("Insufficient balance in the advertiser account.");
    }

    advertiser.adCoins -= advertisement.maxCost;

    const advertiserRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Advertiser');
    await advertiserRegistry.update(advertiser);

    // Update the status of the slot sale status
    slot.onSale = false;
    const slotRegistry = await getAssetRegistry('org.nitk.webadvertising.Slot')
    await slotRegistry.update(slot);

    // Create the lockIn asset
    return getAssetRegistry('org.nitk.webadvertising.LockIn')
        .then(function(result){
            let factory = getFactory();
            let newLockIn = factory.newResource(
                'org.nitk.webadvertising',
                'LockIn',
                tx.lockIn.lockInId
            );
            newLockIn.slot = slot;
            newLockIn.advertisement = advertisement;
            newLockIn.lockedCoins = advertisement.maxCost;
            return result.add(newLockIn);
        })
}

/**
 * Function to add adCoins to a user
 * @param {org.nitk.webadvertising.BuyAdCoins} tx
 * @transaction
 */
async function buyAdCoins(tx) {
    // Get the buyer
    let buyer = tx.buyer;

    // Increase the adCoins
    buyer.adCoins += tx.amount;

    // Get the user registry
    const userRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Advertiser');
    await userRegistry.update(buyer);
}

/**
 * Function to sell adCoins to a user
 * @param {org.nitk.webadvertising.SellAdCoins} tx
 * @transaction
 */
async function sellAdCoins(tx) {
    // Get the seller
    let seller = tx.seller;

    // Check if the amount exists
    if (tx.amount > seller.adCoins) {
        throw new Error("Insufficient balance in the wallet.");
    }
    // Decrease the adCoins
    seller.adCoins -= tx.amount;

    // Get the user registry
    const userRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Publisher');
    await userRegistry.update(seller);
}


/**
 * Function to register a click on the advertisement
 * @param {org.nitk.webadvertising.AdClick} tx
 * @transaction
 */
async function adClick(tx) {
    // Get the lock-in to be modified
    let lockIn = tx.lockIn;

    // Get the cost of the advertisement
    let cost = lockIn.slot.cost;

    // Deduct the balance from the lock-in box and add it to publisher
    lockIn.lockedCoins -= cost;
    let publisher = lockIn.slot.owner;
    publisher.adCoins += cost;

    if (lockIn.lockedCoins < cost) {
        
        let advertiser = lockIn.advertisement.advertiser;
        advertiser.adCoins += lockIn.lockedCoins;
        lockIn.lockedCoins = 0;

        const advertiserRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Advertiser');
        await advertiserRegistry.update(advertiser);

        // Update the status of the slot sale status
        let slot = lockIn.slot;
        slot.onSale = true;
        const slotRegistry = await getAssetRegistry('org.nitk.webadvertising.Slot')
        await slotRegistry.update(slot);

        // Expire the deal
        const dealExpiry = getFactory().newEvent('org.nitk.webadvertising', 'DealExpiry');
        dealExpiry.lockIn = lockIn;
        emit(dealExpiry);

        const lockInRegistry = await
        getAssetRegistry('org.nitk.webadvertising.LockIn');
        await lockInRegistry.remove(lockIn);

    }
    
    const publisherRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Publisher');
    await publisherRegistry.update(publisher);

    const lockInRegistry = await
        getAssetRegistry('org.nitk.webadvertising.LockIn');
    await lockInRegistry.update(lockIn);
}

/**
 * Function to cancel the deal
 * @param {org.nitk.webadvertising.CancelDeal} tx
 * @transaction
 */
async function cancelDeal(tx) {
    let lockIn = tx.lockIn;
    let advertiser = lockIn.advertisement.advertiser;

    advertiser.adCoins += lockIn.lockedCoins;
    lockIn.lockedCoins = 0;

    const advertiserRegistry = await 
        getParticipantRegistry('org.nitk.webadvertising.Advertiser');
    await advertiserRegistry.update(advertiser);

    let slot = lockIn.slot;
    slot.onSale = true;
    const slotRegistry = await getAssetRegistry('org.nitk.webadvertising.Slot')
    await slotRegistry.update(slot);

    // Expire the deal
    const dealExpiry = getFactory().newEvent('org.nitk.webadvertising', 'DealExpiry');
    dealExpiry.lockIn = lockIn;
    emit(dealExpiry);
    
    const lockInRegistry = await
        getAssetRegistry('org.nitk.webadvertising.LockIn');
    await lockInRegistry.remove(lockIn);
}

/**
 * Function to create transaction for slot verification
 * @param {org.nitk.webadvertising.SlotVerification} tx 
 */

async function slotVerification(tx) {
    let slot = tx.slot;
    slot.verifiedSlot = true;

    const assetRegistry = await
        getAssetRegistry('org.nitk.webadvertising.Slot');
    await assetRegistry.update(slot);
}

/**
 * Function to create transaction for website verification
 * @param {org.nitk.webadvertising.WebsiteVerification} tx 
 */

async function websiteVerification(tx) {
    let slot = tx.slot;
    slot.verifiedWebsite = true;

    const assetRegistry = await
        getAssetRegistry('org.nitk.webadvertising.Slot');
    await assetRegistry.update(slot);
}

